import Nerv, { Component } from 'nervjs'
import { View, Text, Input } from '@tarojs/components'
import './index.scss'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <Input focus value='123'></Input>
      </View>
    )
  }
}
